<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            #tabla{
                display:table;
                border-collapse: separate;
                border-spacing: 10px;
            }
            #tabla>div{
                display:table-cell;
                width:50px;
                height: 50px;
                text-align: center;
                vertical-align: middle;
                font-size: 2em;
                color:#ccc;
                border: 1px solid black;
            }
            
            #ficha{
                background-image: url('./imgs/circle.svg');
                width:50px;
                height: 50px;
            }
        </style>
    </head>
    <body>
        <?php
        $numero= mt_rand(1,6);
        ?>
        <img src="./imgs/<?= $numero?>.svg" alt="alt"/>
        <div id="tabla">
        <?php
        
        for($c=1;$c<=6;$c++){
            
        ?>
        
        <div>
        <?php
            if($c==$numero){
        ?>
        <img id="ficha" src="./imgs/circle.svg">
        <?php
            }
            echo $c;
        ?>
        </div>
        <?php
        
        }
        ?>
        </div>
    </body>
</html>
