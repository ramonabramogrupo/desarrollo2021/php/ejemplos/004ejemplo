<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            #tabla{
                display:table;
                border-collapse: separate;
                border-spacing: 10px;
            }
            #tabla>div{
                display:table-cell;
                width:50px;
                height: 50px;
                text-align: center;
                vertical-align: middle;
                font-size: 2em;
                color:#ccc;
                border: 1px solid black;
            }
            
            #posicion{
                background-image: url('./imgs/circle.svg');
                background-size: 100% 100%;
            }
        </style>
    </head>
    <body>
        <?php
            $numero1= mt_rand(1,6); 
            $numero2=mt_rand(1,6); 
            $suma=$numero1+$numero2;
        ?>
        
        <img src="./imgs/<?= $numero1?>.svg">
        <img src="./imgs/<?= $numero2?>.svg">
        
        <div id="tabla">
        <?php
        for($c=1;$c<=12;$c++){
            if($c==$suma){  
        ?>
            <div id="posicion"><?= $c // esta celda es la que tiene la ficha?></div> 
        <?php
            }else{
         ?>
            <div><?= $c // esto son las celdas sin ficha ?></div>
         <?php    
            }
        ?>
        
        <?php
        }
        ?>
        </div>
        
    </body>
</html>