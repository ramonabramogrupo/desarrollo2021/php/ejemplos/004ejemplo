<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            #tabla{
                display:table;
                border-collapse: separate;
                border-spacing: 10px;
            }
            #tabla>div{
                display:table-cell;
                width:50px;
                height: 50px;
                text-align: center;
                vertical-align: middle;
                font-size: 2em;
                color:#ccc;
                border: 1px solid black;
            }
            
            #ficha{
                background-image: url('./imgs/circle.svg');
                width:50px;
                height: 50px;
            }
        </style>
    </head>
    <body>
        <?php
        $numeros=[
            mt_rand(1,6),
            mt_rand(1,6)
        ];
        
        $suma=$numeros[0]+$numeros[1];
        ?>
        <img src="./imgs/<?= $numeros[0] ?>.svg" >
        <img src="./imgs/<?= $numeros[1] ?>.svg" >
        <div id="tabla">
        <?php
        
        for($c=1;$c<=12;$c++){
            
        ?>
        
        <div>
        <?php
            if($c==$suma){
        ?>
        <img id="ficha" src="./imgs/circle.svg">
        <?php
            }
            echo $c;
        ?>
        </div>
        <?php
        
        }
        ?>
        </div>
    </body>
</html>
