<?php
    # calculando el numero de dados
    $numeroDados=mt_rand(1,10);
    
    # es un array con los valores de cada dado
    $dados=[];
    $sumaDados=0;
    
    for($c=0;$c<$numeroDados;$c++){
        $dados[$c]=mt_rand(1,6);
        $sumaDados+=$dados[$c];
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
         <style>
            #tabla{
                display:table;
                border-collapse: separate;
                border-spacing: 10px;
            }
            
            #tabla>div{
                display:table-row;
            }
            
            #tabla>div>div{
                display:table-cell;
                width:50px;
                height: 50px;
                text-align: center;
                vertical-align: middle;
                font-size: 2em;
                color:#ccc;
                border: 1px solid black;
            }
            
            #posicion{
                background-image: url('./imgs/circle.svg');
                background-size: 100% 100%;
            }
        </style>
    </head>
    <body>
      <div>
      <?php
      // dibujo los dados
      foreach ($dados as $valor) {
          echo '<img src="imgs/' . $valor . '.svg">';
      }
      /* 
      #Esto es lo mismo pero con un for
      for($c=0;$c<$numeroDados;$c++){
          echo '<img src="imgs/' . $dados[$c] . '.svg">';
      }
       */
      ?>
      </div>
      <?php
      // dibujando el tablero
        $celdas=60;
        $columnas=10;
        $filas=$celdas/$columnas;
        
        echo '<div id="tabla">';
        for($nfila=1,$c=1;$nfila<=$filas;$nfila++){
            echo "<div>";
            for($ncolumna=1;$ncolumna<=$columnas;$ncolumna++,$c++){
                if($c==$sumaDados){
                    echo '<div id="posicion">' . $c . '</div>';
                }else{
                    echo "<div>$c</div>";
                }
            }
            echo "</div>";
        }
        
        echo '</div>';
      ?>
        
    </body>
</html>
